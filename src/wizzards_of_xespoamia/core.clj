(ns wizzards-of-xespoamia.core
  (:gen-class)
  (require [lanterna.screen :as s]
           [overtone.at-at :refer [now mk-pool every stop-and-reset-pool!]]))

(def buildings
  (atom
    {:gold-mines {:amount 1
                  :price 1
                  :income 1
                  :speed 1000
                  :cost-exp 1.6}}))

(def last-action
  (atom
    {:time (now)
     :gold 0}))

(defn elapesd-time-since-last-action []
  (- (now) (get @last-action :time)))

(defn gold-earned[]
  (let [et (elapesd-time-since-last-action)
        cycles (Math/floor (/ et (get-in @buildings [:gold-mines :speed])))]
    (* cycles (get-in @buildings [:gold-mines :income]))))


(defn current-gold []
  (+ (get-in @last-action [:gold]) (gold-earned)))

(defn can-buy [building]
  (>= (current-gold) (get-in @buildings [building :price])))

(defn building-price-color [building]
  (if (can-buy building) :green :red))

(defn buy-building [building]
  (when (can-buy building)
    (swap! last-action assoc
      :time (now)
      :gold (- (current-gold) (get-in @buildings [building :price])))
    (swap! buildings update-in [building :amount] inc)
    (swap! buildings update-in [building :price] * (get-in @buildings [building :cost-exp]))))

(defn -main []
  (def scr (s/get-screen))
  (def my-pool (mk-pool))

  (s/start scr)

  (every 100
    (fn []
      (s/clear scr)

      (s/put-string scr 2 1 (str "Gold: " (format "%.2f" (float (current-gold)))))

      (s/put-string scr 2 4 (str "(G)old Mines: " (get-in @buildings [:gold-mines :amount])))
      (s/put-string scr 2 5 (str "Price: " (format "%.2f" (float (get-in @buildings [:gold-mines :price])))) {:fg (building-price-color :gold-mines)})

      (s/put-string scr 2 28 "E(x)it!")
      (s/redraw scr))
    my-pool)

  (loop []
    (let [key (s/get-key-blocking scr)]
      (cond
        (= key \g)
        (buy-building :gold-mines))
      (when (not= key \x) (recur))))


  (stop-and-reset-pool! my-pool :strategy :kill)
  (s/stop scr))
